const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtratcPlugin = require('mini-css-extract-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const autoprefixer = require('autoprefixer');
const ImageminPlugin = require('imagemin-webpack-plugin').default

module.exports = (env, argv) => {
    const devMode = argv.mode !== 'production';

    return {
            entry: ['babel-polyfill', './src/index.js'],
            output: {
                filename: devMode ? 'js/bundle.js' : 'js/bundle.[hash].js',
                path: path.resolve(__dirname, 'public'),
                chunkFilename: devMode ? 'js/[id].js' : 'js/chunk.[id].[hash].js',
                //publicPath: '/'
            },
            module: {
                rules: [
                    {
                        test:/\.js$/,
                        exclude: /node_modules/,
                        loader: 'babel-loader'
                    },
                    {
                        test: /\.s?css$/,
                        use: [
                            {
                                loader: MiniCssExtratcPlugin.loader,
                                options: {
                                    hmr: devMode
                                }
                            }, 
                            {
                                loader: 'css-loader',
                                options: {
                                    sourceMap: true
                                }
                            },
                            {
                                loader: "postcss-loader",
                                options: {
                                    plugins: [
                                        autoprefixer()
                                    ],
                                    sourceMap: true
                                }
                            },
                            {
                                loader: 'sass-loader',
                                options: {
                                    sourceMap: true
                                }
                            }
                            
                        ]

                    },
                    {
                        test: /\.(eot|svg|ttf|woff|woff2)$/,
                        loader: 'file-loader'
                    },
                    {
                        test: /\.(png|jpg|gif|svg)$/,
                        loader: 'file-loader'
                    }
                ]
            },
            plugins: [
                new CleanWebpackPlugin({
                    cleanOnceBeforeBuildPatterns: ['**/*']
                }),
                new MiniCssExtratcPlugin({
                    filename: devMode ? 'css/style.css' : 'css/style.[contenthash].css',
                    chunkFilename: devMode ? 'css/[id].css' : 'css/[id].[contenthash].css',
                }),
                new HtmlWebpackPlugin(
                    {
                    template: 'src/index.html'
                }),
                new CopyPlugin({
                    patterns: [
                        {from: 'src/assets/font', to: 'assets/font'},
                        {from: 'src/assets/img', to: 'assets/img'}

                    ]
                }),
                new ImageminPlugin({
                    disable: devMode, 
                    test: /\.(jpe?g|png|gif|svg)$/i,
                    pngquant: {
                      quality: '95-100'
                    }
                })
            ],
            optimization: {
                splitChunks: {
                    chunks: 'all',
                    minSize: 30000
                }
            },
            devServer: {
                contentBase: path.resolve(__dirname, 'public'),
                port: 3000,
                open: true,
                publicPath: '/'
            },
            devtool: devMode ? 'source-map' : '' 
    }
};